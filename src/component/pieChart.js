import React, { Component } from 'react'
import { Pie } from 'react-chartjs-2';
import axios from 'axios'

export default class pieChart extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            title: [],
            point: [],
        }
    }

    componentDidMount() {
        axios.get('http://localhost:5000/intentData')
            .then(res => this.setState({
                data: res.data,
                title: res.data.map(pie => pie.intent),
                point: res.data.map(pie => pie.point),
            }))
            .catch(error => {
                console.log(error);
            })
    };

    render() {
        console.log(this.state.title)
        return (
            <div>
                <div className="title-content">
                    <h1>
                        Overview
                    </h1>
                </div>
                <Pie
                    data={{
                        labels: this.state.title,
                        datasets: [{
                            data: this.state.point,
                            backgroundColor: [
                                "#2ecc71",
                                "#3498db",
                                "#95a5a6",
                                "#9b59b6",
                                "#f1c40f",
                                "#e74c3c",
                            ],
                        }],
                    }}
                />
            </div>
        )
    }
}
