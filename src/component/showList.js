import React, { Component } from 'react'
import axios from 'axios';
import InputTag from './InputTag '
import './style.css';

export default class showList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            synonyms: [],
        }
    }

    componentDidMount() {
        axios.get('http://localhost:5000/entity')
            .then(res => this.setState({
                data: res.data,
                synonyms: res.data.map(list => list.synonyms)
            }))
            .catch(error => {
                console.log(error);
            })
    };

    render() {

        return (
            <div>
                <div className="title-content">
                    <h1>
                        Phrase
                    </h1>
                </div>
                <table className="table">
                    <thead className="thead-light">
                        <tr>
                            <th className="title-value">Value</th>
                            <th className="title-synonyms">Synonyms</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.data.map((list, index) => (
                            <tr key={index}>
                                <td>{list.value}</td>
                                <td>
                                    <InputTag synonyms={list.synonyms} />
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div >
        )
    }
}