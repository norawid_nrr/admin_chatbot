import React, { Component } from 'react'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import PieChart from './pieChart';
import List from './showList';
import { BarChartSharp, Ballot, FormatListBulletedSharp, ForumSharp, AccountTree, Details, Language } from '@material-ui/icons';

export default class sidebar extends Component {
    constructor(props) {
        super(props)

        this.state = {
            active: '',
        }
        this.addActiveClass = this.addActiveClass.bind(this)
    }

    addActiveClass(e) {
        const clicked = e.target.id;
        if (this.state.active === clicked) {
            this.setState({ active: '' });
        } else {
            this.setState({ active: clicked })
        }

        console.log(e)
    }

    render() {
        return (
            <Router>
                <nav>
                    <div className="sidebar">
                        <h3 className="menu-title">
                            <Link to="/" className="link-title" >Admin chatbot</Link>
                        </h3>
                        <ul>
                            <div className="toggle-click" onClick={this.addActiveClass}>
                                <Link to='/data' className={`${this.state.active === 'data' ? 'selected' : ''} link`} >
                                    <li className={`${this.state.active === 'data' ? 'selected' : ''} data`} id="data">
                                        <BarChartSharp className="icon" />
                                        Data Analytics
                                    </li>
                                </Link>
                            </div>
                            <div className="toggle-click" onClick={this.addActiveClass}>
                                <Link to='/#' className={`${this.state.active === 'intent' ? 'selected' : ''} link`}>
                                    <li className={`${this.state.active === 'intent' ? 'selected' : ''} intent`} id="intent">
                                        <FormatListBulletedSharp className="icon" />
                                        Intents
                                    </li>
                                </Link>
                            </div>
                            <div className="toggle-click" onClick={this.addActiveClass}>
                                <Link to='/entity' className={`${this.state.active === 'entity' ? 'selected' : ''} link`}>
                                    <li className={`${this.state.active === 'entity' ? 'selected' : ''} entity`} id="entity">
                                        <Ballot className="icon" />
                                    Entities
                                    </li>
                                </Link>
                            </div>
                            <div className="toggle-click" onClick={this.addActiveClass}>
                                <Link to='/#' className={`${this.state.active === 'conversation' ? 'selected' : ''} link`}>
                                    <li className={`${this.state.active === 'conversation' ? 'selected' : ''} conversation`} id="conversation">
                                        <ForumSharp className="icon" />
                                        Conversation Flow
                                    </li>
                                </Link>
                            </div>
                            <div className="toggle-click" onClick={this.addActiveClass}>
                                <Link to='/#' className={`${this.state.active === 'business' ? 'selected' : ''} link`}>
                                    <li className={`${this.state.active === 'business' ? 'selected' : ''} business`} id="business">
                                        <AccountTree className="icon" />
                                        Business Logic
                                    </li>
                                </Link>
                            </div>
                            <div className="toggle-click" onClick={this.addActiveClass}>
                                <Link to='/#' className={`${this.state.active === 'rule' ? 'selected' : ''} link`}>
                                    <li className={`${this.state.active === 'rule' ? 'selected' : ''} rule`} id="rule">
                                        <Details className="icon" />
                                        Rule-Based
                                    </li>
                                </Link>
                            </div>
                            <div className="toggle-click" onClick={this.addActiveClass}>
                                <Link to='/#' className={`${this.state.active === 'nlp' ? 'selected' : ''} link`}>
                                    <li className={`${this.state.active === 'nlp' ? 'selected' : ''} nlp`} id="nlp">
                                        <Language className="icon" />
                                        NLP
                                    </li>
                                </Link>
                            </div>
                        </ul>
                    </div>


                    <div className="router-content">
                        <div className="container">
                            <div className="content-wrapper">
                                <Switch>
                                    <Route path='/data'>
                                        <PieChart />
                                    </Route>
                                    <Route path='/entity'>
                                        <List />
                                    </Route>
                                </Switch>
                            </div>
                        </div>
                    </div>
                </nav>
            </Router >
        )
    }
}
