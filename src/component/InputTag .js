import React, { Component } from 'react';

class InputTag extends Component {
    constructor(props) {
        super(props)

        this.state = {
            tags: []
        }

        this.inputKeyDown = this.inputKeyDown.bind(this);
    }

    componentDidMount() {
        this.setState({
            tags: this.props.synonyms
        })
    }

    inputKeyDown(e) {
        const val = e.target.value;
        if (e.key === 'Enter' && val) {
            //Check if tag already exists
            if (this.state.tags.find(tag => tag.toLowerCase() === val.toLowerCase())) {
                return;
            }
            this.setState({
                tags: [...this.state.tags, val]
            });
            this.tagInput.value = null;
        } else if (e.key === 'Backspace' && !val) {
            this.removeTag(this.state.tags.length - 1);
        }

        console.log(this.state.tags)
    }

    removeTag(i) {
        const newTags = [...this.state.tags];
        newTags.splice(i, 1);
        this.setState({ tags: newTags })
    }

    render() {
        return (
            <div>
                <ul className="input-tag-item">
                    {
                        this.state.tags.map((tag, i) => (
                            <li className="tag-items" key={tag}>
                                {tag}
                                <button type="button" onClick={() => this.removeTag(i)}>x</button>
                            </li>
                        ))}
                    <li className="input-item-field">
                        <input type="text" onKeyDown={this.inputKeyDown} ref={c => { this.tagInput = c }} placeholder="Add More" />
                    </li>
                </ul>
            </div>
        );
    }
}

export default InputTag;