import React from 'react';
import logo from './logo.svg';
import './App.css';
import Sidebar from './component/sidebar';


function App() {
  return (
    <div className="App">
      <Sidebar />
    </div>
  );
}

export default App;
